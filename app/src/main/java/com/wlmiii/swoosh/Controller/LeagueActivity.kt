package com.wlmiii.swoosh.Controller

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.wlmiii.swoosh.Model.Player
import com.wlmiii.swoosh.R
import com.wlmiii.swoosh.Utilities.EXTRA_PLAYER
import kotlinx.android.synthetic.main.activity_league.*

class LeagueActivity : BaseActivity() {

  private var player = Player("", "")

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_league)

    mensLeagueButton.setOnClickListener {
      womensLeagueButton.isChecked = false
      coedLeaugeButton.isChecked = false
      player.league = "mens"
    }

    womensLeagueButton.setOnClickListener {
      mensLeagueButton.isChecked = false
      coedLeaugeButton.isChecked = false
      player.league = "womens"
    }

    coedLeaugeButton.setOnClickListener {
      mensLeagueButton.isChecked = false
      womensLeagueButton.isChecked = false
      player.league = "coed"
    }

    leagueNextButton.setOnClickListener {
      if (player.league != "") {
        val skillActivity = Intent(this, SkillActivity::class.java)
        skillActivity.putExtra(EXTRA_PLAYER, player)
        startActivity(skillActivity)
      } else {
        Toast.makeText(this, "Please select a league.", Toast.LENGTH_SHORT).show()
      }
    }
  }
}
