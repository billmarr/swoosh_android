package com.wlmiii.swoosh.Controller

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.wlmiii.swoosh.Model.Player
import com.wlmiii.swoosh.R
import com.wlmiii.swoosh.Utilities.EXTRA_PLAYER
import kotlinx.android.synthetic.main.activity_skill.*

class SkillActivity : AppCompatActivity() {

  private lateinit var player: Player

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_skill)
    player = intent.getParcelableExtra(EXTRA_PLAYER)

    beginnerSkillButton.setOnClickListener {
      ballerSkillButton.isChecked = false
      player.skill = "beginner"
    }

    ballerSkillButton.setOnClickListener {
      beginnerSkillButton.isChecked = false
      player.skill = "baller"
    }

    finishButton.setOnClickListener {
      if (player.skill != "") {
        val finishSearchActivity = Intent(this, FinishActivity::class.java)
        finishSearchActivity.putExtra(EXTRA_PLAYER, player)
        startActivity(finishSearchActivity)
      } else {
        Toast.makeText(this, "Please select a skill level.", Toast.LENGTH_SHORT).show()
      }
    }
  }
}
